class CashierLine:
    lstLine = []

    def addCustomer(self, strName):
        self.lstLine.append(strName)

    def processCustomer(self):
        strReturnName = self.lstLine[0]
        self.lstLine.remove(strReturnName)
        return strReturnName

    def printStatus(self):
        strReturn = ""
        for itr in range(len(self.lstLine)):
            strReturn += self.lstLine[itr] + " "
        return strReturn

blnLoop = True
line = CashierLine()
while blnLoop:
    strName = input("Enter customer name :")
    if strName == ".":
        break
    elif strName == "->":
        print("Processed :", line.processCustomer())
        print("Line :", line.printStatus())
    else:
        line.addCustomer(strName)
        print("Line :", line.printStatus())
print("Number of remaining customers :", len(line.lstLine))