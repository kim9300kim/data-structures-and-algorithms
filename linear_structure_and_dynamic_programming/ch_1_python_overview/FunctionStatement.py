numA = 1
numB = 2


def add(numParam1, numParam2):
    return numParam1 + numParam2


def multiply(numParam1, numParam2):
    return numParam1 * 2, numParam1 * 3


def increase(numParam1, step=1):
    return numParam1 + step


numC = add(numA, numB)
numD, numE = multiply(numA, numB)
numF = increase(numA, 5)
numG = increase(numA)

lambdaAdd = lambda numParam1, numParam2: numParam1 + numParam2

numH = lambdaAdd(numA, numB)

print(numC, numD, numE, numF, numG, numH)
