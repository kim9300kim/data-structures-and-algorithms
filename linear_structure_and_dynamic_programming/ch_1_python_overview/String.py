strTest = "Hello World! ISE"
print(strTest)
strTestComp = "Hello World! ISE"
print(strTestComp, strTest == strTestComp)

print(strTest[0], strTest[1])
print(strTest[-1], strTest[-2])

print(len(strTest))
print(strTest + " " + "Dept")
print(strTest*2)
print("ISE" in strTest)
print("ISE" not in strTest)