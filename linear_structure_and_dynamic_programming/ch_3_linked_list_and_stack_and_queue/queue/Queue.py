from linear_structure_and_dynamic_programming.ch_3_linked_list_and_stack_and_queue.linked_list import SinglyLinkedList


class Queue(object):
    lstInstance = SinglyLinkedList()

    def dequeue(self):
        return self.lstInstance.removeAt(0)

    def enqueue(self, value):
        self.lstInstance.insertAt(value, self.lstInstance.getSize())

    def isEmpty(self):
        return self.lstInstance == 0


# queue = Queue()
# queue.enqueue("a")
# queue.enqueue("b")
# queue.enqueue("c")
#
# print(queue.dequeue())
# print(queue.dequeue())
# print(queue.dequeue())
