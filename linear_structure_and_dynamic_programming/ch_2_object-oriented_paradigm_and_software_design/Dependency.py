class Calculator:
    def calculateSomething(self):
        return ...


class Engineer:
    def drwFloorplan(self):
        calc = Calculator()
        value = calc.calculateSomething()
        return value
