class GeneticAlgorithm:

    def performEvolution(self, numIterations, numOffstrings):
        population = self.creatInitialPopulation()

        for itr in range(numIterations):

            offstring = {}

            for itr2 in range(numOffstrings):
                p1, p2 = self.selectParents()
                offstring[itr2] = self.crossoverParents(p1, p2)
                offstring[itr2] = self.mutation(offstring[itr2])

            self.substritutePopulation(population, offstring)

        mostFittest = self.findBestSolution(population)

        return mostFittest